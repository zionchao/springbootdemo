package com.ff.springboot01.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ff.springboot01.dao.DepartmentMapper;
import com.ff.springboot01.domin.Department;

@RestController
public class DepartmentController {
	
	@Autowired
	private DepartmentMapper departmentMapper;
	
	@GetMapping("getDepartmentInfo")
	public Department getDepartmentInfo() {
		Department department = this.departmentMapper.getById(1);
		return department;
	}

}
