package com.ff.springboot01.controller;

import java.util.Date;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ff.springboot01.domin.User;

@Controller
@RequestMapping("/test")
public class TestController {
	
	@GetMapping("hello")
	public String helloworld(Map<String,Object> map) {
		map.put("msg", "Hello Thymeleaf");
		return "hello";
	}
	
	@GetMapping("/testcorf")
	public String helloworld2() {
		return "test";
	}

}
