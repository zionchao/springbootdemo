package com.ff.springboot01.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ff.springboot01.dao.StudentRepository;
import com.ff.springboot01.domin.Student;
import com.ff.springboot01.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {
	
	@Autowired
	StudentRepository studentRepository;

	@Override
	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public void removeStudent(Integer id) {
		studentRepository.delete(id);

	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		return studentRepository.save(student);
	}

	@Override
	public Student getStudentById(Integer id) {
		// TODO Auto-generated method stub
		return studentRepository.findOne(id);
	}

	@Override
	public List<Student> listStudents() {
		// TODO Auto-generated method stub
		return studentRepository.findAll();
	}

	@Override
	public List<Student> listStudentsByNameAndAgeAndSex(String name, String sex, Integer age1, Integer age2) {
		if(name!=null)
			return studentRepository.findByNameLike(name);
		else if(sex!=null)
			return studentRepository.findBySex(sex);
		else
			return studentRepository.findByAgeBetween(age1, age2);
	}

}
