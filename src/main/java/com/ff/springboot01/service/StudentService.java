package com.ff.springboot01.service;

import java.util.List;

import com.ff.springboot01.domin.Student;

public interface StudentService {
	
	Student saveStudent(Student student);
	
	void removeStudent(Integer id);
	
	Student updateStudent(Student student);
	
	Student getStudentById(Integer id);
	
	List<Student> listStudents();
	
	List<Student> listStudentsByNameAndAgeAndSex(String name,String sex,Integer age1,Integer age2);

}
