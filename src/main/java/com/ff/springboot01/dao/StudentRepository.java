package com.ff.springboot01.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ff.springboot01.domin.Student;


@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
	
	List<Student> findBySex(String sex);
	
	//根据学生的姓模糊查询学生的详细信息,该方法名等同于where name like ?1; 
	@Query("select s from Student s where name like CONCAT('%',:name,'%')")
	List<Student> findByNameLike(@Param("name") String name);
	
	List<Student> findByAgeBetween(Integer age1,Integer age2);
	
	 //如果感觉JPA不能满足需求也可以自定义sql;注意这里占位符?后面是数字1,select后面不能使用*,必须要给表取个别名,否则会报错
	@Query("select s from Student s where s.name=?1")
	List<Student> getStudent();

}
