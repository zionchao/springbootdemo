package com.ff.springboot01.dao;

import org.apache.ibatis.annotations.Mapper;

import com.ff.springboot01.domin.Department;

@Mapper
public interface DepartmentMapper {
	
	public void insert(Department department);
	
	public Department getById(Integer id);
	
	public void update(Department department);
	
	public void deleteById(Integer id);
	
}
