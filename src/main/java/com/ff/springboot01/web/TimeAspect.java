package com.ff.springboot01.web;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TimeAspect {
	
	@Around(value="execution(* com.ff.springboot01.controller.TestController.*(..))")	
	public Object method(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println("=====Aspect处理=======");
		
		Object[] args=pjp.getArgs();
		for (Object arg : args) {
            System.out.println("参数为:" + arg);
        }
		
		long start = System.currentTimeMillis();

        Object object = pjp.proceed();

        System.out.println("Aspect 耗时:" + (System.currentTimeMillis() - start));

        return object;
	}
	
	
//	@Before(value="execution(* com.ff.springboot01.controller.TestController.helloworld(..))")	
//	public void method(JoinPoint joinPoint) throws Throwable {
//		System.out.println("=====Aspect处理=======");
//		
//		long start = System.currentTimeMillis();
//
//		 MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
//	      Method method = methodSignature.getMethod();
//	     System.out.println("Hello" + method.getName());
//
//        System.out.println("Aspect 耗时:" + (System.currentTimeMillis() - start));
//
//	}

}
