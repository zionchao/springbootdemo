package com.ff.springboot01.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter{
	
	@Autowired
	private TimeInterceptor timeInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO Auto-generated method stub
//		super.addInterceptors(registry);
		
		 //addPathPatterns 用于添加拦截规则
        //excludePathPatterns 用于排除拦截
      /*  registry.addInterceptor(new MyInterceptor()).addPathPatterns("/**")
            .excludePathPatterns("/hlladmin/login") //登录页
            .excludePathPatterns("/hlladmin/user/sendEmail") //发送邮箱
            .excludePathPatterns("/hlladmin/user/register") //用户注册
            .excludePathPatterns("/hlladmin/user/login"); //用户登录
*/		
		
		registry.addInterceptor(timeInterceptor);
	}
	
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
          @Override
          public void addCorsMappings(CorsRegistry registry) {
              registry.addMapping("/fastjson/**")
                      .allowedOrigins("http://localhost:8080");// 允许 8088 端口访问
          }
        };
	}
	
/*	@Bean
	public HttpMessageConverters fastJsonHttpMessageConverters() {
		
		FastJsonHttpMessageConverter fastJsonHttpMessageConverter=new FastJsonHttpMessageConverter();
		
		FastJsonConfig fastJsonConfig=new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
		
		fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
		
		HttpMessageConverter<?> converter=fastJsonHttpMessageConverter;
		
		return new HttpMessageConverters(converter);
		
	}
	
	@Bean
	public ServletRegistrationBean servletRegistrationBan() {
		return new ServletRegistrationBean(new ServletTest(),"/servletTest");
	}
	
	@Bean
	public FilterRegistrationBean timeFilter() {
		FilterRegistrationBean registrationBean=new FilterRegistrationBean();
		
		TimeFilter timeFilter=new TimeFilter();
		registrationBean.setFilter(timeFilter);
		
		List<String> urls=new ArrayList<>();
		urls.add("/*");
		registrationBean.setUrlPatterns(urls);
		
		return registrationBean;
	}
	@Bean
	public ServletListenerRegistrationBean<ListenerTest> servletListenerRegisterBean(){
		return new ServletListenerRegistrationBean<ListenerTest>(new ListenerTest());
		
	}*/


}
