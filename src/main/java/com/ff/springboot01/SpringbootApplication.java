package com.ff.springboot01;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.ff.springboot01.web.ListenerTest;
import com.ff.springboot01.web.ServletTest;
import com.ff.springboot01.web.TimeFilter;


@SpringBootApplication
@EnableAspectJAutoProxy
public class SpringbootApplication implements ServletContextInitializer{

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		
		servletContext.addServlet("servletTest", new ServletTest()).addMapping("/servletTest");
		
		servletContext.addFilter("timeFilter", new TimeFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),
				true, "/*");
		
		servletContext.addListener(new ListenerTest());
	}

}
